# SHACL validation of TERN plot ontology

1. Download shaclvalidator from TopBraid
https://github.com/TopQuadrant/shacl
2. Run shaclvalidation    
shaclvalidate.sh -datafile TEST_CASE_FILE -shapesfile SHAPEFILE

## Source file contents
* plot.shapes.ttl 
    * geometryShape
    targets the geometry of a location, as well as ensuring the location is within Australia
    * locationShape
    targets <http://linked.data.gov.au/dataset/corveg/location/LOCATION> class
    * observationShape
    targets sosa:Observation, constraints include the object of sosa:hasFeatureOfInterest is a plot:Site
    * OCShape
    targets ssn-ext:ObservationCollection. similar to observationShape but with additional constraint of ssn-ext:hasMember
    * siteShape
    targets a TERN Site.
    * taxonShape
    targets plot:SiteTaxon class

* conceptScheme.shapes.ttl
    * conceptSchemeShape
    targets skos:ConceptScheme class, specify fields for a plot conceptScheme
    * conceptShape
    targets skos:Concept class. similar to conceptScheme. topConceptOf conceptScheme

* dwcTaxon.shapes.ttl
    * dwcCollectionShape
    targets skos:Collection
    * taxonShape
    Darwin Core Taxon

* strata.shapes.ttl
    * strataShape
    targets ObservationCollection which contain dct:type ogroup:Strata
    * stratumShape
    validates plot:SiteStratum
    * strataObservationResultShape
    validates the result of a strata obervation
    * strataObservationShape
    validates sosa:Observation. constraints include sosa:hasFeatureOfInterest of plot:SiteStratum

* stratataxon.shapes.ttl
    * siteStrataTaxonObservationShape
    targets strata taxon observation. sosa:Observation    
    * siteStrataTaxonObservationResultShape
    validates results of a strata taxon observation
    * siteStrataTaxonShape
    targets plot:SiteStratumTaxon
    * strataTaxonShape
    targets ObservationCollection which contain dct:type ogroup:StrataTaxa
    
* structure.shapes.ttl
    this file was initialy created to test the custom sh:target using sparql. the structure observation collection can also be validated using observationShape in plot.shapes.ttl
    * structureShape
    validates ssn-ext:ObservationCollection with ogroup:Structure
    * structureObservationShape
    validates sosa:Observation hasResult of structure result
    * structureResultShape
    validates structure results
    